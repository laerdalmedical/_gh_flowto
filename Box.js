
module.exports = {

 Box : function (_id, xa, ya) {
    var me = {};
    var endNodeId = "end";
    var id = _id; 
		var xAxes = xa;
		var yAxes = ya;
		var yCent;
    var xPos;
    var yPos;
    var width;
    var height;
		var xCent;
		var yCent;
    var txtContent = "<div>Box</div>";
    var dest;
    var boxClass;
    var xAxisId;
    var yAxisId;
    var cornerRadius = "10";
    var renderedRect = {};
    var arrowMarkup;
    var alignArrowWithToElement = "no";
    var drawArrows = true;
    var href;
    var yMargin = 5;
    var xMargin = 5;

	// round to max 2 decimal places
	var trunc2 = function (num) {
		return Math.round(num * 100) / 100;
	}

    me.getClass = function() { return "Box"; }

    me.setId = function (_id) {
        id = _id.trim();
    };
    me.getId = function (_id) {
        return id;
    };
    me.setDest = function (d) {
        dest = d;
    };
    me.getDest = function (d) {
        return dest;
    };
    me.setClass = function (c) {
        boxClass = c;
    };
    me.setHeight = function (_height) {
        height = parseInt(_height,10); 
    };
    me.getHeight = function () {
        return height;
    };
    me.setWidth = function (_width) {
        width = parseInt(_width,10); 
    };
    me.setX = function (x) {
        xPos = parseInt(x,10); 
    };
    me.getX = function () {
        return xPos;
    };
    me.setXAxis = function (axisId) {
        xAxisId = axisId;
        xPos = xAxes.getPosFor(axisId);
    };
    me.getXAxis = function () {
        return xAxisId;
    };
    me.setYAxis = function (axisId) {
        yAxisId = axisId;
        yPos = yAxes.getPosFor(axisId);
    };
    me.getYAxis = function () {
        return yAxisId;
    };
    me.setContent = function (someContent) {
        txtContent = someContent;
    };
    me.setCornerRadius = function (_cornerRadius) {
        cornerRadius = _cornerRadius; 
    };
    me.setAlignArrowWithToElement = function (_alignArrowWithToElement) {
        alignArrowWithToElement = _alignArrowWithToElement.toLowerCase(); 
    };
    me.getAlignArrowWithToElement = function () {
        return alignArrowWithToElement; 
    };
    me.suppressOutgoingArrows = function () {
        drawArrows = false; 
    };
    me.getRenderedRect = function () {
        return renderedRect;
    };
    me.preRender = function () {
      // offset x and y so that the box is centered on both axes
      xCent = xPos - (width/2);
      yCent = yPos - (height/2);
      renderedRect = {x: xCent, y: yCent, actWidth: width, actHeight: height};
    }
    me.setHref = function (h) {
      href = h;
    }
    me.setArrowCoords = function (am) {
      arrowMarkup = am;
    }
    me.getPathways = function () {
      return [];
    }
    me.render = function () {
        var boxMarkup = '';
        // TODO - check that we have everything before rendering
        if (href) {
          boxMarkup += '<a id="'+id+'" xmlns:xlink="http://www.w3.org/1999/xlink" aria-label="'+txtContent+'" xlink:href="' + href +'">';
        } else {
          boxMarkup ='<g id="'+id+'" tabindex="0" role="listitem">';
          boxMarkup += '<g id="'+id+'-pathway" class="pathway" ';
          boxMarkup += ' laerdal:flowto="'+(dest ? dest : endNodeId)+'">';
          if (arrowMarkup && drawArrows) {
            boxMarkup += '<path id="'+id+'-ar" d="'+arrowMarkup+'"/>';
          }
          boxMarkup += '</g>';
        }
        boxMarkup += '<rect id="'+id+'-bg" class="'+boxClass+'" x="'+xCent+'" y="'+yCent+'" width="' + width + '" height="'+ height + '" rx="' + cornerRadius + '" ry="' + cornerRadius + '"/>';
        boxMarkup += '<foreignObject width="' + (width - (xMargin*2)) + '" height="' + (height - (yMargin*2)) + '" transform="matrix(1 0 0 1 '+ (xCent + xMargin) +' ' + (yCent + yMargin) + ')"><div xmlns="http://www.w3.org/1999/xhtml">';
        boxMarkup += txtContent;
        boxMarkup += '</div></foreignObject>';
        if (href) {
          boxMarkup += '</a>';
        } else {
          boxMarkup += '</g>';
        }
        return boxMarkup;
    }
    return me;
  }
}
