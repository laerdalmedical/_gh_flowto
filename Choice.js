var Pathway = require("./Pathway.js");

module.exports = {

  Choice: function (_id, xa, ya) {
    var me = {};
    var endNodeId = "end";
    var id = _id;
		var xAxes = xa;
		var yAxes = ya;
    var xPos;
    var yPos;
    var width;
    var height;
    var xCent;
    var yCent;
    var txtContent = "<div>Choice</div>";
    var dest;
    var choiceClass;
    var yAxisId;
    var hexagonCorners = [];
    var arrowData = [];
    var arrowMarkup;

    var yMargin = 5;
    var xMargin = 5;
    var pointWidth = 20;

    var pathways = [];
    var renderedRect = {};
    var activePathway = {};
    var alignArrowWithToElement = "no";

    // round to max 2 decimal places
    var trunc2 = function (num) {
      return Math.round(num * 100) / 100;
    }
    var preRenderPathways = function () {
      pathways.forEach(function (pathway) {
        pathway.preRender(yPos + height);
      })
    }

    me.getClass = function () {
      return "Choice";
    }

    me.setId = function (_id) {
      id = _id;
    };
    me.getId = function () {
      return id;
    };
    me.setDest = function (d) {
      dest = d;
    };
    me.setClass = function (c) {
      choiceClass = c;
    };
    me.setHeight = function (_height) {
      height = trunc2(parseInt(_height, 10));
    };
    me.getHeight = function () {
      return height;
    };
    me.setWidth = function (_width) {
      width = trunc2(parseInt(_width, 10));
    };
    me.setX = function (x) {
        xPos = parseInt(x,10); 
    };
    me.getX = function () {
        return xPos;
    };
    me.setXAxis = function (axisId) {
        xAxisId = axisId;
        xPos = xAxes.getPosFor(axisId);
    };
    me.getXAxis = function () {
        return xAxisId;
    };
    me.setYAxis = function (axisId) {
        yAxisId = axisId;
        yPos = yAxes.getPosFor(axisId);
    };
    me.getYAxis = function () {
        return yAxisId;
    };
    me.setPointWidth = function (pw) {
      pointWidth = parseInt(pw, 10);
    }
    me.setContent = function (someContent) {
      txtContent = someContent;
    };
    me.getRenderedRect = function () {
      return renderedRect;
    };
    me.setAlignArrowWithToElement = function (_alignArrowWithToElement) {
      alignArrowWithToElement = _alignArrowWithToElement.toLowerCase();
    };
    me.getAlignArrowWithToElement = function () {
      return alignArrowWithToElement;
    };
    me.addPathway = function (pathway) {
      pathways.push(pathway);
    }
    me.getActivePathway = function () {
      activePathway = pathways[pathways.length - 1];
      return activePathway;
    }
    me.setActivePathway = function (pathway) {
      pathways[pathways.length - 1] = pathway;
      activePathway = null;
    }
    me.getPathways = function () {
      return pathways;
    }

    me.preRender = function ()  {
      var halfWidth = trunc2(width / 2);
      var halfHeight = trunc2(height / 2);
      xCent = trunc2(xPos - halfWidth);
      yCent = trunc2(yPos - halfHeight);
      var right = xCent + width;
      var bottom = yCent + height;
      var innerLeft = xCent + pointWidth;
      var innerRight = right - pointWidth;

      hexagonCorners.push([xCent, yPos]);
      hexagonCorners.push([innerLeft, yCent]);
      hexagonCorners.push([innerRight, yCent]);
      hexagonCorners.push([right, yPos]);
      hexagonCorners.push([innerRight, bottom]);
      hexagonCorners.push([innerLeft, bottom]);

      renderedRect = {
        x: xCent,
        y: yCent,
        actWidth: width,
        actHeight: height
      };
    }
    me.setArrowCoords = function (am) {
      arrowData.push(am);
    }

    me.render = function () {
      var choiceMarkup = '\n';
      var tWidth = width - (xMargin * 2);
      var tHeight = height - (yMargin * 2);
      preRenderPathways();
      // TODO - check that we have everything before rendering
      choiceMarkup = '\t<g id="' + id + '" tabindex="0" role="listitem">';
      choiceMarkup += ' <text style="fill:transparent" aria-live="off" x="800" y="300"> </text>';
      choiceMarkup += '<polygon id="' + id + '-bg" class="' + choiceClass + '" points="';
      hexagonCorners.forEach(function (corner) {
        choiceMarkup += corner.join(",");
        choiceMarkup += " ";
      });
      choiceMarkup += '" />';

      choiceMarkup += '<foreignObject width="' + tWidth + '" height="' + tHeight + '" transform="matrix(1 0 0 1 ' + (xCent + xMargin) + ' ' + (yCent + yMargin) + ')">';
      choiceMarkup += '<div xmlns="http://www.w3.org/1999/xhtml">\n';
      choiceMarkup += txtContent;
      choiceMarkup += '</div></foreignObject>';
      
      arrowData.forEach(function (thisArrowData) {
        choiceMarkup += '<path id="TEST" class="arrow" d="' + thisArrowData + '"/>';
      });

      pathways.forEach(function (p) {
        choiceMarkup += p.render();
      });
      choiceMarkup += '</g>';
      return choiceMarkup;
    }

    return me;
  }
}
