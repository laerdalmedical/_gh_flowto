
module.exports = {
  Arrow : function (_id) {
      var me = {};
      var id = _id;
      var pathData = "";
      var arrowHeadOffset = 15;
      var directionalArrowHeadOffset = 8.5;

      me.getClass = function() { return "Arrow"; }

      me.inRange = function(start, end, range) {
          if (start > end) {
            return end+range > start;
          } else {
            return start+range > end;
          }
      }

      me.preRender = function (renderRect) {
        var startX = renderRect.startX;
        var startY = renderRect.startY;
        var endX = renderRect.endX;
        var endY = renderRect.endY;
        //

        if (startX === endX || me.inRange(startX, endX, 5)) {
          if (endY > startY) {
            endY -= arrowHeadOffset;
          } else if (endY < startY) {
            endY += arrowHeadOffset;
          }
        } else {
          if (startY === endY || me.inRange(startY, endY, 5)) {
            if (endX > startX) {
              endX -= arrowHeadOffset;
            } else if (endX < startX) {
              endX += arrowHeadOffset;
            } 
          } else {

            if (endX > startX) {
              endX -= directionalArrowHeadOffset;
            } else if (endX < startX) {
              endX += directionalArrowHeadOffset;
            } 

            if (endY > startY) {
              endY -= directionalArrowHeadOffset;
            } else if (endY < startY) {
              endY += directionalArrowHeadOffset;
            }
  
          }
        } 
        //
        pathData = 'M'+ startX + ',' + startY + ' ' + endX + ',' + endY;
      }

      me.getArrowMarkup = function () {
        return pathData;
      }

      me.render = function (renderRect) {
          var arrowMarkup = '\n';
          arrowMarkup = '<g id="' + id + '-pathway" class="pathway">';
          arrowMarkup +='\t<path id="'+id+'-ar" d="M'+ renderRect.startX + ',' + renderRect.startY + ' ' + renderRect.endX + ',' + renderRect.endY + '"/>\n';
          arrowMarkup += '\n</g>\n';
          return arrowMarkup;
      }
      return me;
  }
}
