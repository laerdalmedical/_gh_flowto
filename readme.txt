
This tool generates flow chart algo's from the .ft files in the "ft" folder

The output from the run is placed in the "svg_out" folder..


How to run:
-----------
node parser.js


After run:
----------
We need to manually copy some SVG tags into the PediatricSepticShockAlgorithm.svg in the svg_out folder

Open the HourPieChartsForUseInALS_AdultSuspectedStrokeAlgorithm.txt in your favourite editor.. 
Copy the content from HourPieChartsForUseInALS_AdultSuspectedStrokeAlgorithm.txt to line just before "<!--- elements" in ALS_AdultSuspectedStrokeAlgorithm.svg
Save the file ALS_AdultSuspectedStrokeAlgorithm.svg

Open the PediatricSepticShockAlgorithm.svg in your favourite editor.. 
Copy the content from FirstHourLineForUseInPALS_PediatricSepticShockAlgorithm.txt to line just before "<!--- elements" in PALS_PediatricSepticShockAlgorithm.svg
Save the file PALS_PediatricSepticShockAlgorithm.svg



How to deploy in ALS PALS:
--------------------------
Copy the generated ALS svg's from svg_out folder to the ../podium-product-auberson\auberson\deploy\esim\html5\assets\images
Copy the generated PALS svg's svg_out folder to the ../podium-product-auberson\pachelbel\deploy\esim\html5\assets\images
Copy the "CSS" folder in svg_out to both ../podium-product-auberson\pachelbel\deploy\esim\html5\assets\images and ../podium-product-auberson\auberson\deploy\esim\html5\assets\images
Copy the "fonts" folder in svg_out to both ../podium-product-auberson\pachelbel\deploy\esim\html5\assets\images and ../podium-product-auberson\auberson\deploy\esim\html5\assets\images
Copy the "js" folder in svg_out to both ../podium-product-auberson\pachelbel\deploy\esim\html5\assets\images and ../podium-product-auberson\auberson\deploy\esim\html5\assets\images
Copy the "svg_other" folder in svg_out to both ../podium-product-auberson\pachelbel\deploy\esim\html5\assets\images and ../podium-product-auberson\auberson\deploy\esim\html5\assets\images