
module.exports = {
  Axes : function (m) {
      var me = {};
      var fullSize = m;
      var axes = {};
      //
      me.addAxis = function (identifier, pcPos) {
        var pcInt = (typeof pcPos === "string") ? parseInt(pcPos, 10) : pcPos;
        if (typeof pcInt !== "number") {
          console.log("crap parameter given to addAxis: " + pcPos);
          return;
        }
        axes[identifier] = (fullSize / 100) * pcInt;
      };
      me.getPosFor = function (identifier) {
        return axes[identifier];
      };
      me.getIt = function () {
        return fullSize;
      };
      me.getAxes = function () {
        return axes;
      };
      return me;
  }
}
