// a non-focusable flowchart box used to label pathway arrows
module.exports = {

  Pathway: function (_id, xa, ya) {
    var me = {};
    var endNodeId = "end";
    var id = _id;
		var xAxes = xa;
		var yAxes = ya;
    var xPos;
    var yPos;
    var width;
    var height;
    var txtContent = "<div>Pathway</div>";
    var dest;
    var boxClass = "lgr";
    var yAxisId;
    var renderedRect = {};
    var cornerRadius = "30";
    var alignArrowWithToElement = "no";

    var yMargin = 2;
    var xMargin = 2;
    var xCent;
    var yCent;
    var arrowMarkup;
    var arrowData = [];

    me.getClass = function () {
      return "Pathway";
    }

    me.setId = function (_id) {
      id = _id.trim();
    };
    me.getId = function (_id) {
      return id;
    };
    me.setDest = function (d) {
      dest = d;
    };
    me.getDest = function (d) {
      return dest;
    };
    me.setClass = function (c) {
      boxClass = c;
    };
    me.setHeight = function (_height) {
      height = parseInt(_height, 10);
    };
    me.getHeight = function () {
      return height;
    };
    me.setWidth = function (_width) {
      width = parseInt(_width, 10);
    };
    me.setX = function (x) {
        xPos = parseInt(x,10); 
    };
    me.getX = function () {
        return xPos;
    };
    me.setXAxis = function (axisId) {
        xAxisId = axisId;
        xPos = xAxes.getPosFor(axisId);
    };
    me.getXAxis = function () {
        return xAxisId;
    };
    me.setYAxis = function (axisId) {
        yAxisId = axisId;
        yPos = yAxes.getPosFor(axisId);
    };
    me.getYAxis = function () {
        return yAxisId;
    };
    me.setCornerRadius = function (_cornerRadius) {
      cornerRadius = _cornerRadius;
    };
    me.setAlignArrowWithToElement = function (_alignArrowWithToElement) {
      alignArrowWithToElement = _alignArrowWithToElement.toLowerCase();
    };
    me.getAlignArrowWithToElement = function () {
      return alignArrowWithToElement;
    };
    me.setContent = function (someContent) {
      txtContent = someContent;
    };
    me.getRenderedRect = function () {
      return renderedRect;
    };
    me.preRender = function () {
      xCent = xPos - (width / 2);
      yCent = yPos - (height / 2);
      renderedRect = {
        x: xCent,
        y: yCent,
        actWidth: width,
        actHeight: height
      };
    }
    me.setArrowCoords = function (am) {
      arrowData.push(am);
    }
    me.render = function (yPos) {

      var boxMarkup = '';
      var i = 1;
      // TODO - check that we have everything before rendering
      boxMarkup = '<g id="' + id + '" laerdal:flowto="' + (dest ? dest : endNodeId) + '" class="pathway" role="listitem">';      
      arrowData.forEach(function (thisArrowData) {
        boxMarkup += '<path id="' + id + '_ar' + i + '" d="' + thisArrowData + '"/>';
        i++;
      });
      boxMarkup += '<rect id="' + id + '-bg" class="' + boxClass + '" x="' + xCent + '" y="' + yCent + '" width="' + width + '" height="' + height + '" rx="' + cornerRadius + '" ry="' + cornerRadius + '"/>';
      boxMarkup += '<foreignObject width="' + (width - (xMargin * 2)) + '" height="' + (height - (yMargin * 2)) + '" transform="matrix(1 0 0 1 ' + (xCent + xMargin) + ' ' + (yCent + yMargin) + ')"><div xmlns="http://www.w3.org/1999/xhtml">';
      boxMarkup += txtContent;
      boxMarkup += '</div></foreignObject></g>';
      return boxMarkup;
    }

    return me;
  }
}
