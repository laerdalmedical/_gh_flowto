var prettify = require('./xml-prettify').prettify; // from https://github.com/aishwar/xml-pretty-print
var Box = require("./Box.js");
var Choice = require("./Choice.js");
var Pathway = require("./Pathway.js");
var Arrow = require("./Arrow.js");
var Axes = require("./Axes.js");

module.exports = {
  Chart: function (fw, fh) {
    var me = {};
    var title = "Untitled Flowchart";
    var language = "en-US";
    var elements = [];
    var currentElementType; // (string) the type of the current element we are making (For now, "Box" or "Choice").
    var currentElement; // the current 'element' we are making
    var connectors = [];
    var fullWidth = fw;
    var fullHeight = fh;
    var xAxes = new Axes.Axes(fullWidth);
    var yAxes = new Axes.Axes(fullHeight);
    var yMargin = 70;
    var headerLineSpacing = 57.6;
    var headerOffsetX = 100;
    var headerOffsetY = 816;
    var EcmaScriptPath = "js/accessible_flowchart.js";
    var boilerplateStyles = ["css/fonts.css", "css/algorithms.css"];
    var boilerplateSVGs = [{
        id: "grid",
        href: "svg_other/grid.svg"
      }, {
        id: "aha",
        href: "svg_other/aha.svg"
      }
    ];
    var images = [];
    var startBoxId = "";
    var endNodeId = "end";
    //
    var buildBoilerplateHead = function () {
      var titleOffsetY = 0;
      var head = '<?xml version="1.0" encoding="utf-8"?>';
      var titleChunks = title.split(" ");
      var headerTop = fullHeight - ((titleChunks.length + 1) * headerLineSpacing)
      boilerplateStyles.forEach(function (href) {
        head += '<?xml-stylesheet href="' + href + '" type="text/css"?>';
      });
      head += '<svg ';
      head += 'version="1.1" ';
      head += 'xmlns="http://www.w3.org/2000/svg" ';
      head += 'xmlns:xlink="http://www.w3.org/1999/xlink" ';
      head += 'xmlns:laerdal="http://www.laerdal.com" ';
      head += 'width="'+fullWidth+'" height="'+fullHeight+'" ';
      head += 'x="0px" y="0px" ';
      /*head += 'viewBox="0 0 ' + fullWidth + ' ' + fullHeight + '" ';*/
      /*head += 'style="enable-background:new 0 0 ' + fullWidth + ' ' + fullHeight + ';" ';*/
      head += 'xml:space="preserve" ';
      head += 'xml:lang="' + language + '" ';
      head += 'role="application">';
      head += buildDefs();
      boilerplateSVGs.forEach(function (data) {
        head += '<image id="' + data.id + '" width="' + fullWidth + '" height="' + fullHeight + '" xlink:href="' + data.href + '" preserveAspectRatio="none" />';
      });
      images.forEach(function (data) {
        head += '<image id="' + data.id + '" width="' + fullWidth + '" height="' + fullHeight + '" xlink:href="' + data.href + '" preserveAspectRatio="none" />';
      });
      head += '<title>' + title + '</title>';
      head += '<g id="header" aria-hidden="true">';
      head += '<text transform="matrix(1 0 0 1 100 ' + headerTop + ')">';
      titleChunks.forEach(function (s) {
        head += '<tspan x="0" y="' + titleOffsetY + '">' + s + '</tspan>';
        titleOffsetY += headerLineSpacing;
      });
      head += '</text></g>';
      return head;
    };

    var buildDefs = function () {
      var defs = "<defs>";
      defs += "<marker id='head' orient='auto' markerWidth='20' markerHeight='4' refX='0.1' refY='2'>"
      defs += "<path d='M0,0 V4 L2,2 Z' fill='#231F20' stroke='none'/>";
      defs += "</marker>";
      defs += "<marker id='head-focus' orient='auto' markerWidth='20' markerHeight='4' refX='0.1' refY='2'>";
      defs += "<path d='M0,0 V4 L2,2 Z' fill='#FFA500' stroke='none'/>";
      defs += "</marker>";
      defs += "</defs>";
      return defs;
    };

    var buildBoilerplateTail = function () {
      var tail = '<g id="end" tabindex="0" >';
      tail += '<text class="minuscule" id="end-txt" transform="matrix(1 0 0 1 320 50)">Start again </text>';
      tail += '<g id="end-pathway" class="pathway" laerdal:flowto="' + startBoxId + '"></g></g>';
      tail += '<g id="trap" tabindex="0"></g>';
      tail += '<script type="text/ecmascript" xlink:href="' + EcmaScriptPath + '"></script></svg>';
      return tail;
    }

    var preRenderElements = function () {
      elements.forEach(function (b) {
        b.preRender();
        b.getPathways().forEach(function (p) {
          p.preRender();
        });
      });
    }
    var renderElements = function () {
      var boxMarkup = "<!--elements-->";
	  boxMarkup += "<g role='list'>";
      elements.forEach(function (b) {
        boxMarkup += b.render();
        //yAxes[b.getYAxis()] = yAxes[b.getYAxis()] + b.getHeight() + yMargin;
      });
	  boxMarkup += "</g>";
      return boxMarkup;
    }

    var preRenderArrows = function () {
      elements.forEach(function (b) {
        //console.log("preRendering pathways");
        b.getPathways().forEach(function (_pathway) {
          var _arrow;
          //
          _arrow = new Arrow.Arrow(b.getId() + '_' + _pathway.getId());
          _arrow.preRender(getStartEndPositions(b, _pathway));

          if (_pathway.getDest != undefined && _pathway.getDest() != '') {
            var destElement = elements.find(function (element) {

                if (element.getId != undefined) {
                  return element.getId() == _pathway.getDest();
                } else {
                  return false;
                }
              });

            if (destElement != undefined) {
              _pathway.setArrowCoords(_arrow.getArrowMarkup());
              _arrow = new Arrow.Arrow(_pathway.getId() + '_' + destElement.getId() + "_");
              _arrow.preRender(getStartEndPositions(_pathway, destElement));
              _pathway.setArrowCoords(_arrow.getArrowMarkup());
            }
          }
        });

        if (b.getDest != undefined && b.getDest() != '') {
          var destElement = elements.find(function (element) {

              if (element.getId != undefined) {
                return element.getId() == b.getDest();
              } else {
                return false;
              }
            });

          if (destElement != undefined) {
            var _arrow = new Arrow.Arrow(b.getId() + '_' + destElement.getId());
            _arrow.preRender(getStartEndPositions(b, destElement));
            b.setArrowCoords(_arrow.getArrowMarkup());
          }
        }

      });
    }

    var getStartEndPositions = function (fromElement, toElement) {
      var fromRect = fromElement.getRenderedRect();
      var toRect = toElement.getRenderedRect();
      var fromX = fromElement.getX();
      var fromY = yAxes.getAxes()[fromElement.getYAxis()];
      var toX = toElement.getX();
      var toY = yAxes.getAxes()[toElement.getYAxis()];

      if (fromElement.getAlignArrowWithToElement != undefined && fromElement.getAlignArrowWithToElement() == "yes") { // Going due south
        return {
          startX: toRect.x + (toRect.actWidth / 2), // xPos of arrow start
          startY: fromRect.y + fromRect.actHeight, // yPos of arrow start
          endX: toRect.x + (toRect.actWidth / 2), // xPos of arrow end
          endY: toRect.y // yPos of arrow end
        }
      }


      if (fromX > toX && fromY < toY) { // Arrow going south west, find the most left-bottom point on fromElement and top-right on toElement
        return {
          startX: fromRect.x, // xPos of arrow start
          startY: fromRect.y + (fromRect.actHeight / 2), // yPos of arrow start
          endX: toRect.x + (toRect.actWidth / 2), // xPos of arrow end
          endY: toRect.y// yPos of arrow end
        }
      }

      if (fromX > toX) { // Arrow going due west, find the most left-bottom point on fromElement and top-right on toElement
        return {
          startX: fromRect.x, // xPos of arrow start
          startY: fromRect.y + (fromRect.actHeight / 2), // yPos of arrow start
          endX: toRect.x + toRect.actWidth, // xPos of arrow end
          endY: toRect.y + (toRect.actHeight / 2) // yPos of arrow end
        }
      }

      if (fromX < toX && fromY < toY) { // Arrow going south east, find the most right-bottom point on fromElement and top-left on toElement
        return {
          startX: fromRect.x + fromRect.actWidth, // xPos of arrow start
          startY: fromRect.y + (fromRect.actHeight / 2), // yPos of arrow start
          endX: toRect.x + (toRect.actWidth / 2), // xPos of arrow end
          endY: toRect.y// yPos of arrow end
        }
      }

      if (fromX < toX) { // Arrow going due east, find the most right-bottom point on fromElement and top-left on toElement
        return {
          startX: fromRect.x + fromRect.actWidth, // xPos of arrow start
          startY: fromRect.y + (fromRect.actHeight / 2), // yPos of arrow start
          endX: toRect.x, // xPos of arrow end
          endY: toRect.y + (toRect.actHeight / 2) // yPos of arrow end
        }
      }

      if (fromY > toY) { // Arrow is going due xx, find the most left-middle point on fromElement and right-middle on toElement
        return {

          startX: fromRect.x + (fromRect.actWidth / 2), // xPos of arrow start
          startY: fromRect.y + fromRect.actHeight, // yPos of arrow start
          endX: toRect.x + (toRect.actWidth / 2), // xPos of arrow end
          endY: toRect.y // yPos of arrow end
        }
      }

      if (fromY < toY) { // Arrow is going due south, find the most right-middle point on fromElement and left-middle on toElement
        return {
          startX: fromRect.x + (fromRect.actWidth / 2), // xPos of arrow start
          startY: fromRect.y + fromRect.actHeight, // yPos of arrow start
          endX: toRect.x + (toRect.actWidth / 2), // xPos of arrow end
          endY: toRect.y// yPos of arrow end
        }
      }

      // Arrow is going due zzzz, find the most bottom-middle point on fromElement and top-middle on toElement
      return {
        startX: fromRect.x + (fromRect.actWidth / 2), // xPos of arrow start
        startY: fromRect.y + fromRect.actHeight, // yPos of arrow start
        endX: toRect.x + (toRect.actWidth / 2), // xPos of arrow end
        endY: toRect.y // yPos of arrow end
      }
    }

    var addAxis = function (whichAxes, identifier, pcPos, fullSize) {
      var pcInt = (typeof pcPos === "string") ? parseInt(pcPos, 10) : pcPos;
      if (typeof pcInt !== "number") {
        console.log("crap parameter given to addAxis: " + pcPos);
        return;
      }
      whichAxes[identifier] = (fullSize / 100) * pcInt;
    }
    /****************************************************/
    //                public interface:
    /****************************************************/
    me.setTitle = function (t) {
      title = t;
    };

    me.setLanguage = function (l) {
      language = l;
    };
    
    me.addImage = function (id_, url_) {
      images.push({
        id: id_,
        href: url_
      });
    }

    me.addXaxis = function (identifier, pcPos) {
      xAxes.addAxis(identifier, pcPos);
    };

    me.addYaxis = function (identifier, pcPos) {
      yAxes.addAxis(identifier, pcPos);
    };

    me.addBox = function (boxId) {
      currentElement = new Box.Box(boxId, xAxes, yAxes);
      currentElementType = "Box";
      elements.push(currentElement);
      startBoxId = (startBoxId || boxId); // first box created becomes the 'start' box
      return currentElement;
    };

    me.addChoice = function (boxId) {
      currentElement = new Choice.Choice(boxId, xAxes, yAxes);
      currentElementType = "Choice";
      elements.push(currentElement);
      return currentElement;
    };

    me.addPathway = function (pathwayId) {
      if (currentElement) {
        currentElement.addPathway(new Pathway.Pathway(pathwayId, xAxes, yAxes));
      }
    };
  //
  me.setPointWidth = function (pw) {
    if (currentElementType == "Choice") {
      currentElement.setPointWidth(pw);
    }
  };
  //
  me.setAlignArrowWithToElement = function (doAlign) {
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setAlignArrowWithToElement(doAlign);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setAlignArrowWithToElement(doAlign);
    }
  };
  me.suppressOutgoingArrows = function () {
    if (currentElementType == "Box") {
      currentElement.suppressOutgoingArrows();
    }
  };
  //
  me.setXaxis = function (axisId) {
    var val = xAxes.getPosFor(axisId);
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setX(val);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setX(val);
    }
  };
  //
  me.setYaxis = function (axisId) {
    var val = yAxes.getPosFor(axisId);
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setYAxis(axisId);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setYAxis(axisId);
    }
  };
  //
  me.setCornerRadius = function (radius) {
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setCornerRadius(radius);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setCornerRadius(radius);
    }
  };
  //
  me.setClass = function (classname) {
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setClass(classname);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setClass(classname);
    }
  };
  //
  me.setHyperlink = function (href) {
    if (currentElementType == "Box") {
      currentElement.setHref(href);
    }
  };
  //
  me.setContent = function (txtContent) {
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setContent(txtContent);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setContent(txtContent);
    }
  };
  //
  me.setWidth = function (width) {
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setWidth(width);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setWidth(width);
    }
  };
  //
  me.setHeight = function (height) {
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setHeight(height);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setHeight(height);
    }
  };
  //
  me.setDest = function (destination) {
    if (currentElementType == "Choice" && currentElement.getActivePathway() != undefined) {
      var currentPathway = currentElement.getActivePathway();
      currentPathway.setDest(destination);
      currentElement.setActivePathway(currentPathway);
    } else {
      currentElement.setDest(destination);
    }
    //console.log("The Flowto property is : ", destination);
    return true;
  };

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    me.render = function () {
      var out = "";
      preRenderElements();
      preRenderArrows();
      //console.log("preRender complete");
      //
      out += buildBoilerplateHead();
      out += renderElements();
      //console.log("element render complete");
      //out += renderArrows();
      out += buildBoilerplateTail();
      //return out;
      //
      prettify.TAB = '  ';
      prettify.WARN = true;
      //prettify.dontIndentTheseElements = ["b", "em", "strong", "span", "li"] // TODO implement this feature in prettifyXML!
      prettyXML = prettify(out);
      return prettyXML;
    };

    return me;
  },

};
