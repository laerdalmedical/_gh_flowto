// standard libs
var fs = require('fs');
var path = require('path');
// 3rd party
var ohm = require('ohm-js'); // from https://github.com/harc/ohm
// our support modules
var Flow = require("./Flowchart.js"); // our flowchart factory
var Box = require("./Box.js");
var Choice = require("./Choice.js");
var Pathway = require("./Pathway.js");
// input related
var inputfileExt = '.ft';
var contents = fs.readFileSync('FlowChartGrammar.ohm');
var ftInputFolder = "./ft/"; // use all the .ft files in this folder as input

// grammar
var myGrammar = ohm.grammar(contents);
var semantics = myGrammar.createSemantics();
// output related:
var svgOutputFolder = "./svg_out/";
var f; // the current flow chart instance
var currentElement; // the current 'element' we are making
var fullWidth = 1900;
var fullHeight = 1070;
// functions:
// clean up all the noise from the parsed token
var groomParsed = function (property) {
  var groomed = property.source.sourceString.substr(property.source.startIdx, property.source.endIdx - property.source.startIdx);
  groomed = groomed.replace(/'/g, ""); // remove all single quotes
  groomed = groomed.replace(/:(:?)/g, "$1"); // convert :: to :
  groomed = groomed.trim();
  return groomed;
}
//
var generateSVG = function (inputFileName, outputFileName) {
  var userInput = fs.readFileSync(inputFileName);
  var m = myGrammar.match(userInput);
  var fileWrittenCallback = function (err) {
    if (err) {
      throw err;
    }
  }
  if (m.succeeded()) {
    semantics(myGrammar.match(userInput)).eval();
    fs.writeFile(outputFileName, f.render(), 'utf-8', fileWrittenCallback);
    console.log('SVG (' + outputFileName + ') created.');
  } else {
    console.log("Error: ", m.message, userInput.toString().substr(m._rightmostFailurePosition, 10));
  }
};
//
// semantic operations
semantics.addOperation('eval', {
  //
  FlowChartStatement: function (_, _, statements, _) {
    f = new Flow.Chart(fullWidth, fullHeight);
    return statements.eval();
  },
  //
  XAxisFunction: function (id, _, _, _, property, _) {
    var axisId = groomParsed(id);
    var axisVal = groomParsed(property);
    f.addXaxis(axisId, axisVal);
    return true;
  },
  //
  YAxisFunction: function (id, _, _, _, property, _) {
    var axisId = groomParsed(id);
    var axisVal = groomParsed(property);
    f.addYaxis(axisId, axisVal);
    return true;
  },
  //
  ImageFunction: function (id, _, _, _, property, _) {
    var imageId = groomParsed(id);
    var url = groomParsed(property);
    f.addImage(imageId, url);
    return true;
  },
  BoxFunction: function (id, _, _, _, properties, _) {
    var boxId = groomParsed(id);
    f.addBox(boxId);
    return properties.eval();
  },
  //
  ChoiceFunction: function (id, _, _, _, properties, _) {
    var boxId = groomParsed(id);
    f.addChoice(boxId);
    return properties.eval();
  },
  //
  PathwayFunction: function (id, _, _, _, properties, _) {
    var pathwayId = groomParsed(id);
    f.addPathway(pathwayId);
    return properties.eval();
  },
  //
  PathwayProperties: function (properties) {
    return properties.eval();
  },
  //
  ChoiceProperties: function (properties) {
    return properties.eval();
  },
  //
  BoxProperties: function (properties) {
    return properties.eval();
  },
  //
  TitleProperty: function (_, property) {
    var title = groomParsed(property)
    f.setTitle(title);
    return true;
  },
  //
  LanguageProperty: function (_, property) {
    var lang = groomParsed(property);
    f.setLanguage(lang);
    return true;
  },

  // ArrowProperty obsolete?
  ArrowProperty: function (_, property) {
    var arrow = groomParsed(property);
    return true;
  },
  //
  PointWidthProperty: function (_, property) {
    var pointWidth = groomParsed(property);
    f.setPointWidth(pointWidth);
    return true;
  },
  //
  AlignArrowWithToElementProperty: function (_, property) {
    var doAlign = groomParsed(property);
    f.setAlignArrowWithToElement(doAlign);
    return true;
  },
  SuppressOutgoingArrows: function (_) {
    f.suppressOutgoingArrows();
    return true;
  },

  XaxisProperty: function (_, property) {
    var axis = groomParsed(property);
    f.setXaxis(axis);
    return true;
  },
  //
  YaxisProperty: function (_, property) {
    var axis = groomParsed(property);
    f.setYaxis(axis);
    return true;
  },
  //
  CornerRadiusProperty: function (_, property) {
    var radius = groomParsed(property);
    f.setCornerRadius(radius);
    return true;
  },
  //
  ClassProperty: function (_, property) {
    var classname = groomParsed(property);
    f.setClass(classname);
    return true;
  },
  //
  TextProperty: function (a, property) {
    var txtContent = groomParsed(property);
    f.setContent(txtContent);
    return true;
  },
  HrefProperty: function (a, property) {
    var href = groomParsed(property);
    f.setHyperlink(href);
    return true;
  },
  //
  ElementWidthProperty: function (_, property) {
    var width = groomParsed(property);
    f.setWidth(width);
    return true;
  },
  //
  ElementHeightProperty: function (_, property) {
    var height = groomParsed(property);
    f.setHeight(height);
    return true;
  },
  //
  FlowtoProperty: function (_, property) {
    var destination = groomParsed(property);
    f.setDest(destination);
    return true;
  },
  _terminal: function () {
    return this.primitiveValue;
  }
});
/**********************************
  This is where it all happens:
***********************************/
fs.readdir(ftInputFolder, function (err, files) {
  if (err)
    throw err;
  console.log(files);
  var inputfiles = files.filter(function (file) {
      return path.extname(file).toLowerCase() === inputfileExt;
    });
  inputfiles.forEach(function (ft) {
    var svgFile = svgOutputFolder + (path.basename(ft, path.extname(ft)) + ".svg");
    var ftFile = ftInputFolder + ft;
    generateSVG(ftFile, svgFile);
  })
})
