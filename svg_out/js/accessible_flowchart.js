﻿(function () {
    "use strict";
    var flowHistory = [];
    var goingBackwards = false;
    var treetop = "vs";
    var choiceBoxMainTextDelay = 50;
    var choiceBoxBranchTextDelay = choiceBoxMainTextDelay + 50;
    var keycodes = {"9": "TAB", "27": "ESC", "37": "LEFT", "39": "RIGHT"};
    var focusable = Array.from(document.querySelectorAll("g[tabindex='0']"));
    var svg = document.querySelector("svg");
    var whiteSpacePattern = /(\r\n|\n|\r|\t)/gm;
    var doubleWhiteSpacePattern = /(\s){2,}/gm;
    var exitSVG = function () {
        if (window.opener) {
            window.opener.focus();
            window.close();
        }
    };
    var scaleSVG = function () {
        // NOTE: 100% disables the Zoom funtion in browsers...
        //svg.setAttribute("width", "100%");
        //svg.setAttribute("height", "100%");
    };
    var extractAccessibleName = function (r) {
        var nl = r.querySelectorAll(":scope > text:not([aria-live]), :scope > foreignObject *");
        var textChildren = Array.from(nl);
        var out = "";
        textChildren.forEach(function (t) {
            out += groomWhiteSpace(t.textContent);
        });
        return out;
    };
	var groomWhiteSpace = function (txt) {
        var preclean = txt.replace(whiteSpacePattern, "");
        return preclean.replace(doubleWhiteSpacePattern, " ");;
	};
    //
    focusable.forEach(function (g) {
        var pathways = Array.from(g.querySelectorAll(".pathway"));
        var chosenPathwayIndex = null;
        var chosenPathway = null;
        var announcer = g.querySelector("text[aria-live]");
        var outlineArrowsInPathway = function (pathway) {
            var arrows = Array.from(pathway.querySelectorAll("path, rect"));
            arrows.forEach(function (a) {
                a.classList.add("focused");
            });
            var boxes = Array.from(pathway.querySelectorAll("rect, polygon"));
            boxes.forEach(function (b) {
                b.classList.add("focused");
            });
        };
        var resetArrowsInPathway = function (pathway) {
            var arrows = Array.from(pathway.querySelectorAll("path"));
            arrows.forEach(function (a) {
                a.classList.remove("focused");
            });
            var boxes = Array.from(pathway.querySelectorAll("rect, polygon"));
            boxes.forEach(function (b) {
                b.classList.remove("focused");
            });
        };
        var focusPathwayByIndex = function (ind) {
            var nl;
            var textChildren;
            if (ind === null) {
                ind = 0;
            } else if (ind < 0 || ind >= pathways.length) {
                // console.log("out of range");
                return;
            }
            if (ind !== chosenPathwayIndex) {
                if (chosenPathway) {
                    resetArrowsInPathway(chosenPathway);
                }
            }
            chosenPathwayIndex = ind;
            chosenPathway = pathways[chosenPathwayIndex];
            if (chosenPathway) {
                if (announcer) {
                    nl = chosenPathway.querySelectorAll("text");
                    textChildren = Array.from(nl);
                    var out = "";
                    textChildren.forEach(function (t) {
                        out += groomWhiteSpace(t.textContent);
                    });
                    window.setTimeout(function () {
                        var id = chosenPathway.id;
                        g.setAttribute("aria-activedescendant", id);
                    }, choiceBoxMainTextDelay);
                    window.setTimeout(function () {
                        announcer.textContent = out;
                    }, choiceBoxBranchTextDelay);
                }
                outlineArrowsInPathway(chosenPathway);
            }
        };
        //
        g.setAttribute("aria-label", extractAccessibleName(g));
        if (pathways.length === 0) {
            return; // no outgoing pathways
        }
        // basic focus and blur:
        g.addEventListener("focus", function (e) {
            if (announcer) {
                announcer.setAttribute("aria-live", "polite");
            }
            focusPathwayByIndex(chosenPathwayIndex);
            /*
            if (flowHistory.length) {
                if (flowHistory[flowHistory.length-1] == e.relatedTarget) {
                    return;
                };
            }
            */
            if (e.target.id === treetop) {
                flowHistory = [];
            }
            flowHistory.push(g);
            var out = "";
            flowHistory.forEach(function (elm) {
                out += (elm.id.charAt(0) + ", ");
            });
        });
        g.addEventListener("blur", function () {
            if (announcer) {
                announcer.setAttribute("aria-live", "off");
                announcer.textContent = " ";
            }
            if (chosenPathway) {
                resetArrowsInPathway(chosenPathway);
                g.removeAttribute("aria-activedescendant");
            }
        });
        g.addEventListener("focusout", function (e) {
            var destId;
            var dest;
            if (goingBackwards) {
                flowHistory.pop();
                dest = flowHistory.pop();
            } else if (chosenPathway) { // forwards according to chosen pathway
                destId = chosenPathway.getAttribute("laerdal:flowto");
                if (destId) {
                    dest = document.getElementById(destId);
                } else {
                    return;
                }
            } else {
                return; // fall back on default tab order (i.e. markup sequence)
            }
            chosenPathway = null;
            if (dest) {
                dest.focus();
                e.preventDefault();
            }
        });
        g.addEventListener("keydown", function (e) {
            var ks = e.keyCode.toString();
            var k = keycodes[ks];
            if (!k) {
                return false;
            }
            if (k === "ESC") {
                exitSVG();
            }
            if (k === "TAB") {
                goingBackwards = e.shiftKey;
                return false;
            }
            if (pathways.length <= 1) {
                return false;
            }
            // two or more pathways to handle
            if (k === "LEFT") {
                focusPathwayByIndex(chosenPathwayIndex - 1);
            } else if (k === "RIGHT") {
                focusPathwayByIndex(chosenPathwayIndex + 1);
            } else {
                return false;
            }
            e.preventDefault();
            return true;
        });
    });
    document.addEventListener("keydown", function (e) {
        var ks = e.keyCode.toString();
        var k = keycodes[ks];
        if (!k) {
            return false;
        }
        if (k === "ESC") {
            exitSVG();
        }
    });
    scaleSVG();
}());